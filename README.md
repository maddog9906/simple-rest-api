# simple-rest-api
Simple Rest API app written in .NET CORE 3.0

## Project Structure
* Configs - Configurations objects. Usually `XXXXXSetting`.
* Controllers - `XXXXController`
* Entities
    * DTOs - DTOS, POCOs, VOs etc..
    * Enumerations
    * Models - Database models
* Extensions
* Filters - Implementations of `IStartFilter`
* Helpers - Helpers, utils etc.
* Repositories - `XXXXRepository` 
* Scripts
    * DbMigrations - Database migration scripts
* Services - `XXXXService`    

## Project Facts

#### Project is written using vs code, using remote container.

* https://cloudblogs.microsoft.com/industry-blog/en-gb/cross-industry/2019/06/21/use-vs-code-remote-development-to-run-blazor-in-a-docker-container/

* https://anthonychu.ca/post/experiment-vscode-remote-development-containers/

#### The chosen database is MySQL. 

#### DbUp is chosen as the db migration tool over EF as my preference is to have migration scripts in SQL.

* https://medium.com/cheranga/database-migrations-using-dbup-in-an-asp-net-core-web-api-application-c24ccfe0cb43

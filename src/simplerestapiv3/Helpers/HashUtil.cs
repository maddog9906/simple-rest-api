using System;
using System.Security.Cryptography;

namespace SimpleRestAPIv3.Utils {
    public class HashUtil
    {
        private const int SaltSize = 16;
        private const int HashSize = 20;
        private const int ITERATIONS = 10000;

        public static bool IsSupported(string hashString)
        {
            return hashString.Contains("APIHASH.V1.");
        }

        public static string Hash(string password)
        {
            // Create salt
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[SaltSize]);

            // Create hash
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, ITERATIONS);
            var hash = pbkdf2.GetBytes(HashSize);

            // Combine salt and hash
            var hashBytes = new byte[SaltSize + HashSize];
            Array.Copy(salt, 0, hashBytes, 0, SaltSize);
            Array.Copy(hash, 0, hashBytes, SaltSize, HashSize);

            // Convert to base64
            var base64Hash = Convert.ToBase64String(hashBytes);

            // Format hash with extra information
            return string.Format("APIHASH.V1.{0}.{1}", ITERATIONS, base64Hash);
        }

        public static bool Verify(string password, string hashedPassword)
        {
            // Check hash
            if (!IsSupported(hashedPassword))
            {
                throw new NotSupportedException("Hashtype not supported!!!! ");
            }

            // Extract iteration and Base64 string
            var splittedHashString = hashedPassword.Replace("APIHASH.V1.", "").Split('.');
            var iterations = int.Parse(splittedHashString[0]);
            var base64Hash = splittedHashString[1];

            // Get hash bytes
            var hashBytes = Convert.FromBase64String(base64Hash);

            // Get salt
            var salt = new byte[SaltSize];
            Array.Copy(hashBytes, 0, salt, 0, SaltSize);

            // Create hash with given salt
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = pbkdf2.GetBytes(HashSize);

            // Get result
            for (var i = 0; i < HashSize; i++)
            {
                if (hashBytes[i + SaltSize] != hash[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
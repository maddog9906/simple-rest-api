using System.Collections.Generic;

namespace SimpleRestAPIv3.Models.DTOs
{
    public class ApiResponse<T>
    {
        private ApiResponse()
        {
            success = true;
        }

        public bool success { get; private set; }   
        public T result { get; private set; }

        public class Builder
        {
            private ApiResponse<T> _apiResponse;

            public Builder()
            {
                _apiResponse = new ApiResponse<T>();
            }

            public Builder WithResult(T result)
            {
                _apiResponse.result = result;
                return this;
            }

            public ApiResponse<T> Build()
            {
                return _apiResponse;
            }
        }

    }

    public class ApiErrorResponse 
    {
        private ApiErrorResponse()
        {
            success = false;
            errors = new List<ApiError>();
        }

        public bool success { get; private set; }
        public List<ApiError> errors { get; private set; }

        public class Builder
        {
            private ApiErrorResponse _apiErrorResponse;

            public Builder()
            {
                _apiErrorResponse = new ApiErrorResponse();
            }

            public Builder AddError(ApiError apiError)
            {
                _apiErrorResponse.errors.Add(apiError);
                return this;
            }

            public ApiErrorResponse Build()
            {
                return _apiErrorResponse;
            }
        }
    }

    public class ApiError
    {
        public ApiError(string code, string description)
        {
            this.code = code;
            this.description = description;
        }

        public string code { get; private set; }
        public string description { get; private set; }
    }
}
using System;

namespace SimpleRestAPIv3.Models.DTOs
{
    public class AccountDTO
    {
        private AccountDTO(){}

        public string email { get; private set; }
        public string firstName { get; private set; }
        public string lastName { get; private set; }
        public string contact { get; private set; }
        public string password { get; private set; }
        public string role { get; private set; }
        public string status { get; private set; }
        public Guid workflowId { get; private set; }

        public class Builder
        {
            private AccountDTO _accountDTO;

            public Builder()
            {
                _accountDTO = new AccountDTO();
                _accountDTO.password = "**********";
            }

            public Builder WithEmail(string email)
            {
                _accountDTO.email = email;
                return this;
            }

            public Builder WithFirstName(string firstName)
            {
                _accountDTO.firstName = firstName;
                return this;
            }

            public Builder WithLastName(string lastName)
            {
                _accountDTO.lastName = lastName;
                return this;
            }

            public Builder WithContact(string contact)
            {
                _accountDTO.contact = contact;
                return this;
            }

            public Builder WithRole(string role)
            {
                _accountDTO.role = role;
                return this;
            }

            public Builder WithStatus(string status)
            {
                _accountDTO.status = status;
                return this;
            }

            public Builder WithWorkflowId(Guid workflowId)
            {
                _accountDTO.workflowId = workflowId;
                return this;
            }

            public AccountDTO Build()
            {
                return _accountDTO;
            }
        } 
    }  
}
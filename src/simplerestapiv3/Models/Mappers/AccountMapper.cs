using System;
using System.Linq;
using System.Collections.Generic;
using SimpleRestAPIv3.Models.DTOs;
using SimpleRestAPIv3.Entities;

namespace SimpleRestAPIv3.Models.Mappers
{
    public class AccountMapper
    {
        public static AccountDTO ToDTO(Account account)
        {
            if (account == null) {
                return null;
            }
            return new AccountDTO.Builder()
                        .WithEmail(account.email)
                        .WithFirstName(account.firstName)
                        .WithLastName(account.lastName)
                        .WithContact(account.contact)
                        .WithRole(account.role)
                        .WithStatus(account.status)
                        .WithWorkflowId(account.workflowId)
                        .Build();
        } 

        public static List<AccountDTO> ToDTOList(List<Account> accounts)
        {
            if (!accounts.Any()){
                return new List<AccountDTO>();
            }
            return accounts.ConvertAll(new Converter<Account, AccountDTO>(ToDTO));
        } 

        /*public static Account FromDTO(AccountDTO dto)
        {
            if (dto == null) {
                return null;
            }
            Account account = new Account
            {
                firstName = dto.firstName,
                lastName = dto.lastName,
                email = dto.email,
                contact = dto.email,
                role = dto.role,
                status = dto.status,
                workflowId = dto.workflowId,
                password = dto.password
            };  
            return account;      
        } */
    }
}
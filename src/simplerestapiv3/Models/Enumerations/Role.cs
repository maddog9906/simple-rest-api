namespace SimpleRestAPIv3.Models.Enumerations
{
    public enum Role
    {
        Admin,
        User
    }
}
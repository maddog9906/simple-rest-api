namespace SimpleRestAPIv3.Models.Enumerations
{
    public enum AccountStatus
    {
        Active,
        Suspended,
        Deleted
    }
}
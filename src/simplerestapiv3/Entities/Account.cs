using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleRestAPIv3.Entities
{
    [Table("account")]
    public class Account : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public Int32 id { get; set; }

        [Required] 
        [MaxLength(50)]
        public string email { get; set; }

        [Column("first_name")]
        [Required] 
        [MaxLength(50)]
        public string firstName { get; set; }

        [Column("last_name")]
        [Required]
        [MaxLength(50)]
        public string lastName { get; set; }

        public string password { get; set; }

        [MaxLength(15)]
        public string contact { get; set; }

        [Required]
        public string role { get; set; }

        [Required]
        public string status { get; set; }

        [Column("workflow_id")]
        public Guid workflowId { get; set; }

        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("version_date")]
        public DateTime versionDate { get; set; }

    }
}
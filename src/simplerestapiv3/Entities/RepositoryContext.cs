using Microsoft.EntityFrameworkCore;
using SimpleRestAPIv3.Entities;

namespace SimpleRestAPIv3.Entities
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions options)
            :base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
    }
}
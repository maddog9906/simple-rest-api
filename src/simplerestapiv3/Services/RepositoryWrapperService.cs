using Microsoft.Extensions.Logging;
using SimpleRestAPIv3.Entities;
using SimpleRestAPIv3.Repositories;

namespace SimpleRestAPIv3.Services
{
    public interface IRepositoryServiceWrapper
    {
        IAccountRepository Account { get; }
    }

    public class RepositoryWrapperService : IRepositoryServiceWrapper
    {
        private RepositoryContext _repoContext;
        private ILoggerFactory _loggerFactory;
        private IAccountRepository _account;

        public RepositoryWrapperService(RepositoryContext repositoryContext, ILoggerFactory loggerFactory)
        {
            _repoContext = repositoryContext;
            _loggerFactory = loggerFactory;
        }

        public IAccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new AccountRepository(_repoContext, _loggerFactory.CreateLogger(typeof(AccountRepository).FullName));
                }

                return _account;
            }
        }
    }
}
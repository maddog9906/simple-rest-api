using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimpleRestAPIv3.Entities;
using SimpleRestAPIv3.Utils;

namespace SimpleRestAPIv3.Repositories
{
    public interface IAccountRepository:IRepositoryBase<Account>
    {
        Task<IEnumerable<Account>> FindAllAsync();
        Task<Account> FindByWorkflowIdAsync(Guid workflowId);
        Task CreateAccount(Account account);
    }

    public class AccountRepository  : RepositoryBase<Account>, IAccountRepository 
    {
        public AccountRepository(RepositoryContext repositoryContext, 
                                 ILogger logger) : base(repositoryContext, logger)
        {
        }

        public async Task<IEnumerable<Account>> FindAllAsync()
        {
            return await FindAll()
                .OrderBy(x => x.lastName).ThenBy(x => x.firstName)
                .ToListAsync();
        }

        public async Task<Account> FindByWorkflowIdAsync(Guid workflowId)
        {
            return await FindByCondition(o => o.workflowId.Equals(workflowId))
                .DefaultIfEmpty(null) 
                .SingleAsync();
        }

        public async Task CreateAccount(Account account)
        {
            // Update relevant fields...
            account.workflowId = Guid.NewGuid();
            account.password = HashUtil.Hash(account.password);
            account.versionDate = DateTime.Now;
            account.createdDate = DateTime.Now;
            
            // To do validation...
            
            //
            Create(account);
            //
            await SaveAsync();
        }
    }
}
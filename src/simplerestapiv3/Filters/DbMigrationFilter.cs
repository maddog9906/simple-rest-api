using System;
using DbUp;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace SimpleRestAPIv3.Filters
{
    public class DbMigrationFilter : IStartupFilter
    {
        private readonly ILogger<DbMigrationFilter> _logger;
        private readonly IConfiguration _configuration;

        public DbMigrationFilter(ILogger<DbMigrationFilter> logger, IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            var connectionString = _configuration.GetConnectionString("MySqlConnection");

            var dbUpgradeEngineBuilder = DeployChanges.To
                .MySqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(typeof(Program).Assembly)
                .WithTransaction()
                .LogToConsole()
                .LogScriptOutput();

            var dbUpgradeEngine = dbUpgradeEngineBuilder.Build();
            if (dbUpgradeEngine.IsUpgradeRequired())
            {
                _logger.LogInformation("Upgrades have been detected. Upgrading database now...");
                var operation = dbUpgradeEngine.PerformUpgrade();
                if (operation.Successful)
                {
                    _logger.LogInformation("Upgrade completed successfully");
                }
                else
                {
                    _logger.LogError("Error happened in the upgrade. Please check the logs");
                }
            }

            return next;
        }
    }
}
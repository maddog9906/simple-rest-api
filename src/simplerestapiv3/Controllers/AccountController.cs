
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SimpleRestAPIv3.Services;
using SimpleRestAPIv3.Entities;
using SimpleRestAPIv3.Models.DTOs;
using SimpleRestAPIv3.Models.Mappers;

namespace SimpleRestAPIv3.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IRepositoryServiceWrapper _repositoryService;

        public AccountController(ILogger<AccountController> logger,
                                 IRepositoryServiceWrapper repositoryService)
        {
            _logger = logger;
            _repositoryService = repositoryService;    
        } 

        [HttpGet]
        public async Task<IActionResult> GetAllAccounts()
        {
            try
            {
                var accounts = await _repositoryService.Account.FindAllAsync();
                return Ok(new ApiResponse<List<AccountDTO>>.Builder()
                                .WithResult(AccountMapper.ToDTOList(accounts.ToList()))
                                .Build());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, new ApiErrorResponse.Builder()
                                            .AddError(new ApiError("ERROR_500", "Generic error"))
                                            .Build());
            }
        }

        [HttpGet("{id}", Name = "AccountById")]
        public async Task<IActionResult> GetAccountById(Guid id)
        {
            try
            {
                var account = await _repositoryService.Account.FindByWorkflowIdAsync(id);

                if (account == null)
                {
                    _logger.LogError($"Account with id: {id}, hasn't been found in db.");
                    return NotFound(new ApiErrorResponse.Builder()
                                            .AddError(new ApiError("ERROR_404", "Account not found"))
                                            .Build());
                }
                else
                {
                    _logger.LogInformation($"Returned account with id: {id}");
                    return Ok(new ApiResponse<AccountDTO>.Builder()
                                    .WithResult(AccountMapper.ToDTO(account))
                                    .Build());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAccountById action: {ex.Message}");
                return StatusCode(500, new ApiErrorResponse.Builder()
                                            .AddError(new ApiError("ERROR_500", "Generic error"))
                                            .Build());
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAccount([FromBody]Account account)
        {
            try
            {
                if (account == null)
                {
                    _logger.LogError("Account object sent from client is null.");
                    return BadRequest("Account object is null");
                }

                if (!ModelState.IsValid || String.IsNullOrEmpty(account.password))
                {
                    _logger.LogError("Invalid account object sent from client.");
                    return BadRequest("Invalid model object");
                }

                await _repositoryService.Account.CreateAccount(account);

                return CreatedAtRoute("AccountById", new { id = account.workflowId }, AccountMapper.ToDTO(account));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, new ApiErrorResponse.Builder()
                                            .AddError(new ApiError("ERROR_500", "Generic error"))
                                            .Build());
            }
        }
    }
}
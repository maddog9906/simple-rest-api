using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SimpleRestAPIv3.Filters;
using SimpleRestAPIv3.Entities;
using SimpleRestAPIv3.Services;

namespace SimpleRestAPIv3.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureDbMigrationService(this IServiceCollection services)
        {
            services.AddTransient<IStartupFilter, DbMigrationFilter>();
        }  

        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config.GetConnectionString("MySqlConnection");
            services.AddDbContext<RepositoryContext>(o => o.UseMySql(connectionString));
        } 

        public static void ConfigureCustomServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<IRepositoryServiceWrapper, RepositoryWrapperService>();
        } 
    }
}


DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `contact` varchar(15) DEFAULT NULL,
  `status` enum('Active','Suspended','Deleted') DEFAULT 'Active',
  `role` varchar(20) NOT NULL,
  `workflow_id` varchar(64) NOT NULL,
  `created_date` datetime(3) NOT NULL,
  `version_date` datetime(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_email` (`email`),
  UNIQUE KEY `uk_workflow` (`workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT FACTORY USER
INSERT INTO `account` (`email`, `first_name`, `last_name`, `password`, `contact`, `status`, `role`, `workflow_id`, `created_date`, `version_date`)
VALUES ('noreply@somecompany.com', 'Admin', 'User', 'APIHASH.V1.10000.eAWaZ825F1bXWkqJhgG1Y9Gz/erPrGZt4cN/TNvsSUCHVT', NULL, 'Active', 'Admin', 'f6f42749-e8be-479b-95a1-cf50e1fa045e', '2019-10-06 23:22:28.302', '2019-10-06 23:22:28.301');
